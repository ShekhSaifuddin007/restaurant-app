<?php

/** @var Factory $factory */

use App\Models\MenuItem;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(MenuItem::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraphs(2, true),
        'price' => $faker->randomFloat(00,99, 999)
    ];
});
